/**
* @file sem.ino
* @author Ondřej Brém
*
* @brief Photo table semestral work for BI-ARD
*
*/

#include <stdlib.h>
#include <math.h>

/**
 * \defgroup const Digital out Pins
 * @{
 */

#define STEP_PIN 5     ///< Digital out pin for motor step definition
#define DIR_PIN 4      ///< Digital out pin for motor direction definition
#define ENABLE_PIN 6   ///< Digital out pin to enable motor definition

#define SHUTTER_PIN 12
#define FOCUS_PIN 13

/**@}*/

/**
 * \defgroup var Default motor parametrs
 * @{
 */
 
#define circle_steps 200 ///< Number of steps in 360 degrees rotation
#define step_size 1.8    ///< Step size

/**@}*/


/**
 * \defgroup states App states
 * @{
 */

#define SETUP_STATE 1
#define RUN_STATE 2
#define PAUSE_STATE 3
#define SETUP_WAIT_SATATE 4

/**@}*/

/**
 * \defgroup vars App default parametrs
 * @{
 */

int fl = 50;
int overlay = 20; ///< Overlay of photos used in calculation of step

int focalLength = 50; ///< default focal length of lens
int senzorSizeX = 36; ///< default senzor width
int senzorSizeY = 24; ///< default sensor height
int shutterSpeed = 2000; ///< default shutter speed used for the delay between moves

int stepSize = 10;
int horizontalSteps = 0; ///< TODO


int state = SETUP_STATE;
String data = ""; ///< data storage for incoming messages
int imageNr = 0;
int totalImages;

/**@}*/

/**
 * @brief Initialize Arduino
 *  Setup all the pins to proper types and zero them out.
 *
 */
void setup() {
  Serial.begin(9600);
  pinMode(DIR_PIN, OUTPUT);
  pinMode(STEP_PIN, OUTPUT);
  pinMode(ENABLE_PIN, OUTPUT);
  pinMode(SHUTTER_PIN, OUTPUT);
  pinMode(FOCUS_PIN, OUTPUT);
  digitalWrite(ENABLE_PIN, 0);
  digitalWrite(DIR_PIN, 0);
  digitalWrite(STEP_PIN, LOW);
  digitalWrite(SHUTTER_PIN, LOW);
  digitalWrite(FOCUS_PIN, LOW);  
  turnBy(90); // test move by the motor
  Serial.println("Setup done");
}

/**
 * @brief Reppeating code
 *  The automaton loop that runs the aplication, based on the current state it either moves the table and shoots the images or talks to the user.
 *
 */
void loop() {
  switch(state){
     case RUN_STATE:
        if (Serial.available()){
          char params = Serial.read();
          if(params == 'p'){
              state = PAUSE_STATE;
              Serial.println("Continue by sending 'c' and quit the operation by sendidng 'q' ");
              break;
          }
        }          
        Serial.println(stepSize);        
        if(horizontalSteps > 0){
          turnBy( stepSize );
          takeImage();
          horizontalSteps--;
        } else {
           state = SETUP_STATE;
        }          
        break;
     case PAUSE_STATE:
        if (Serial.available()){
          char params = Serial.read();
          if(params == 'c'){
              state = RUN_STATE;
              Serial.println("pause by sending 'p'");
              break;
          }
          if(params == 'q'){
              state = SETUP_STATE;
              break;
          }
        }         
        break; 
     case SETUP_STATE:
        Serial.println("Setup your params in following format: {focalLength}/{senzorWidth}/{senzorHeight}q");
        imageNr = 0;
        state = SETUP_WAIT_SATATE;
        break;
     case SETUP_WAIT_SATATE:
     default:        
        if (Serial.available()){
          readParams();
        }        
        break;        
  }
}

/**
 * \defgroup fn Functions
 * @brief Set of support functions for calculations and HW and user interaction 
 * @{
 */

/**
 * @brief Turns the motor by given amount of degrees
 *
 * @param degree int value how many degrees should the motor turn by
 */
void turnBy(int degree){
  int steps = degree/step_size;
  for(int i = 0; i < steps; i++){
     doStep();
  }  
}
/**
 * @brief One step of the motor
 */
void doStep(){
    digitalWrite(STEP_PIN, HIGH);   
    delay(1);                 
    digitalWrite(STEP_PIN, LOW);  
}
/**
 * @brief Fires the cammera shutter
 *
 *  By enabling the propper Digital out pins it triggers the camera shutter mechanism
 *
 */
void pressShutter(){
    digitalWrite(SHUTTER_PIN, HIGH);   
    delay(500);                 
    digitalWrite(SHUTTER_PIN, LOW);    
}
/**
 * @brief Takes image
 *
 *  Waits for the device to come to complete stop and waits for the wibrations to settle down and then triggers the shutter, 
 *  afterwards it waits for the image to be taken. Also counts the nr of images taken.
 *
 */
void takeImage(){
    delay(2000); 
    pressShutter();
    Serial.print("image ");
    Serial.print(imageNr);
    Serial.print(" of ");
    Serial.print(totalImages);
    Serial.println(" taken");
    imageNr++;
    delay(shutterSpeed+2000); 
}
/**
 * @brief Move the camera to next horizontal row
 *
 *  TODO
 *  The function would change the vertical location of the camera to allow for view from top at the photographed object
 *  Would be finished when the HW part is ready for this extension.
 *
 * @param angle int value of the vertical step in degrees
 *
 */
void stepUp(int angle){
  // TODO
   Serial.println("TODO: camera movement in vertical axis"); 
}
/**
 * @brief Reads the input from serial link.
 *
 */
void readParams(){
  char params = Serial.read();  
  if(params == 'q'){
    setParams(data);
    state = RUN_STATE;
    Serial.println("pause by sending 'p'");    
    data = "";
    return;
  } else {
    data += params;
  }
}
/**
 * @brief Parses user input and set parametrs
 *
 *  From the string input it parses all of the parameters needed to calculate the proper movement
 *
 * @param d String containing the data from user input.
 */
void setParams(String d){
  Serial.println(d);
  
  int firstDel = d.indexOf('/');
  int secondDel = d.indexOf('/', firstDel+1);
 
  focalLength = d.substring(0, firstDel).toInt();
  senzorSizeX = d.substring(firstDel+1, secondDel).toInt(); 
  senzorSizeY = d.substring(secondDel+1).toInt(); 
  
  Serial.println( focalLength );
  Serial.println( senzorSizeX );
  Serial.println( senzorSizeY );
  
  stepSize = floor(fov(focalLength, senzorSizeX)*((100-overlay)/(double)100));
  Serial.print("stepSize: ");
  Serial.println(stepSize);
  horizontalSteps = ceil(360/stepSize);  
  totalImages = horizontalSteps;
}
/**
 * @brief Calculates FOV
 *
 * @param focalLength int value of focal length of the used lens
 * @param senzorSize int value senzor size of the used camera
 *
 * @retval int calculated FOV
 */
int fov(int focalLength, int senzorSize){
  Serial.print("fov: ");
  int fov = ceil((2*atan(senzorSize/((double)2*focalLength)))*57296/1000);
  Serial.println(fov);
  return fov;
}

/**@}*/

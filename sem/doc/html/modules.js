var modules =
[
    [ "Digital out Pins", "group__const.html", "group__const" ],
    [ "Default motor parametrs", "group__var.html", "group__var" ],
    [ "App states", "group__states.html", "group__states" ],
    [ "App default parametrs", "group__vars.html", "group__vars" ],
    [ "Functions", "group__fn.html", "group__fn" ]
];
var group__vars =
[
    [ "data", "group__vars.html#ga1393b21435bd418d78d2fb07973af726", null ],
    [ "fl", "group__vars.html#ga7fc40ae9b70bd68d26a6bd0dad9d563d", null ],
    [ "focalLength", "group__vars.html#gaf5071c083efe11ded3d3e9fb13c0e516", null ],
    [ "horizontalSteps", "group__vars.html#ga9054cc54569ac8d9de1331d7620073ef", null ],
    [ "imageNr", "group__vars.html#ga32036d5c63bfee50e2450bb22bc2f337", null ],
    [ "overlay", "group__vars.html#ga3103a0ba6b47735fd39d79f1dec1627f", null ],
    [ "senzorSizeX", "group__vars.html#gaf979f17a36889eafe4a478a7dabb5736", null ],
    [ "senzorSizeY", "group__vars.html#ga299df93fa0d2e2c94240e093b9b4ad6b", null ],
    [ "shutterSpeed", "group__vars.html#ga5a3f8c2b0fcffd28a4871ce538321873", null ],
    [ "state", "group__vars.html#ga89f234133d3efe315836311cbf21c64b", null ],
    [ "stepSize", "group__vars.html#gadfe857b14dacc3123d5c36a0fa44f0b0", null ],
    [ "totalImages", "group__vars.html#ga5ca01a8ef737e136d8532258fa203d0e", null ]
];